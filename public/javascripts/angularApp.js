angular.module('weatherNews', ['ui.router'])
.factory('postFactory', ['$http', function($http)
{
	var o =
	{
		posts: [],
		post: {}
	};

	o.getAll = function()
	{
		$http.defaults.cache = false;
		return $http.get('/posts').success(function(data)
		{
			//console.log('copying data: ' + JSON.stringify(data));
			angular.copy(data, o.posts);
		});
	};

	o.create = function(post)
	{
		return $http.post('/posts', post).success(function(data)
		{
			o.posts.push(data);
		});
	};

	o.upvote = function(post)
	{
		return $http.put('/posts/' + post._id + '/upvote')
		.success(function(data)
		{
			post.upvotes += 1;
		});
	};

	o.getPost = function(id)
	{
		//console.log('getPost called');
		return $http.get('/posts/' + id).success(function(data)
		{
			angular.copy(data, o.post);
			console.log('copied data to o.post' + JSON.stringify(o.post));
		});
	};

	o.addNewComment = function(id, comment)
	{
		return $http.post('/posts/' + id + '/comments', comment);
	};

	o.upvoteComment = function(selPost, comment)
	{
		return $http.put('/posts/' + selPost._id + '/comments/' + comment._id + '/upvote')
		.success(function(data)
		{
			comment.upvotes += 1;
		});
	};

	return o;
}])
.config(
[
	'$stateProvider',
	'$urlRouterProvider',
	function($stateProvider, $urlRouterProvider)
	{
		$stateProvider
		.state('home',
		{
			url: '/home',
			templateUrl: '/home.html',
			controller: 'MainCtrl'
		})
		.state('posts',
		{
			url: '/posts/{id}',
			templateUrl: '/posts.html',
			controller: 'PostCtrl'
		});
		$urlRouterProvider.otherwise('home');
	}
])
.controller('MainCtrl',
[
        '$scope',
        'postFactory',
        function($scope, postFactory)
		{
			postFactory.getAll();

			$scope.posts = postFactory.posts;
			$scope.addPost = function()
			{
				if ($scope.formContent === '')
					return;
				var data = {title:$scope.formContent, upvotes:0, comments:[]};
				//$scope.posts.push(data);
				$scope.formContent='';
				postFactory.create(data);
			};

			$scope.incrementUpvotes = function(post)
			{
				postFactory.upvote(post);
			};

		}
])
.controller('PostCtrl',
[
	'$scope',
	'$stateParams',
	'$location',
	'postFactory', 

	function($scope, $stateParams, $location, postFactory)
	{
		postFactory.getAll();

		console.log('called function inside PostCtrl');
		console.log('$stateParams: ' + JSON.stringify($stateParams));
		console.log('postFactory.posts: ' + JSON.stringify(postFactory.posts));

		if (postFactory.posts.length == 0)
		{
			console.log('the posts have not loaded yet');
			$location.path('/');
			return;
		}

		var mypost = postFactory.posts[$stateParams.id];

		console.log('mypost: ' + JSON.stringify(mypost));
		console.log('id found: ' + mypost._id);

		postFactory.getPost(mypost._id);
		$scope.post = postFactory.post;
		//$scope.post = postFactory.posts[$stateParams.id];
		$scope.addComment = function()
		{
			if($scope.body === '')
				return;
			console.log('about to add new comment');
			console.log('postFactory.post: ' + JSON.stringify(postFactory.post));
			postFactory.addNewComment(postFactory.post._id,
			{
				body: $scope.body
			})
			.success(function(comment)
			{
				mypost.comments.push(comment);
				postFactory.post.comments.push(comment);
			});
			$scope.body = '';
		};
		$scope.incrementUpvotes = function(comment)
		{
			console.log("incrementUp " + postFactory.post._i + " comment " + comment._id);
			postFactory.upvoteComment(postFactory.post, comment);
		};
	}
]);
